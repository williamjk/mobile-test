package test;

import static org.junit.Assert.assertEquals;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;


public class MobileAppTest {

	static String deviceName = "Google Nexus 4 - 5.1.0 -API 22-768x1280";
	AppiumDriver<WebElement> driver;
	DesiredCapabilities capabilities;
	
	@Before
	public void setUp() throws InterruptedException, ExecuteException, IOException {
		
			capabilities = new DesiredCapabilities();

	        DefaultExecutor executor = new DefaultExecutor();
	        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();

	        CommandLine launchEmul = new CommandLine("C:/Program Files/Genymobile/Genymotion/player");
	        launchEmul.addArgument("--vm-name");
	        launchEmul.addArgument("\""+deviceName+"\"");
	        executor.setExitValue(1);
	        executor.execute(launchEmul, resultHandler);
	        Thread.sleep(25000);

	        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");   
	        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "4.3");
	        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
	        capabilities.setCapability(MobileCapabilityType.APP,"C:/Android/TrianguloApp.apk");

	        driver = new AppiumDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"),capabilities){

				public WebElement scrollTo(String arg0) {
					// TODO Auto-generated method stub
					return null;
				}

				public WebElement scrollToExact(String arg0) {
					// TODO Auto-generated method stub
					return null;
				}
	        	
	        };
	        
	      System.out.println("SetUp is successful and Appium Driver is launched successfully");
	}

	@Test
	public void mobileApptest() {

		driver.findElement(By.id("com.eliasnogueira.trianguloapp:id/txtLado1"))
				.sendKeys("3");
		driver.findElement(By.id("com.eliasnogueira.trianguloapp:id/txtLado2"))
				.sendKeys("3");
		driver.findElement(By.id("com.eliasnogueira.trianguloapp:id/txtLado3"))
				.sendKeys("3");
		driver.findElement(
				By.id("com.eliasnogueira.trianguloapp:id/btnCalcular")).click();

		assertEquals("O tri�ngulo � Equil�tero", driver.findElement(By
				.id("com.eliasnogueira.trianguloapp:id/txtResultado")).getText());

	}

	@After
	public void tearDown() throws Exception {
		driver.quit();

	}

}
